"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { Effect } from "../../../game/effect";
import Creature from "../creature";

export default abstract class Humanoid extends Creature {
  static creators = ["krm"];
  static value = 0;
  size!: Calculation;
  condition!: Calculation;
  strength!: Calculation;
  flexibility!: Calculation;
  intelligent!: Calculation;
  charisma!: Calculation;
  wisdom!: Calculation;
  might!: Calculation;
  private _protection = 0;
  private _armor = 0;

  constructor(name: string, type: string[], creators = Humanoid.creators) {
    super(name, ["humanoid"].concat(type), creators);
    this.cost = this.cost + Creature.value;
  }

  showStats() {
    return t(
      this.showBanner,
      t("Cost        ", t(this.cost).fixed(0), " 💰"),
      t("HP          ", t(this.hp).fixed(0), " ❤️"),
      t("Max HP      ", t(this.maxHP.result).fixed(0), " ❤️"),
      t("Size        ", t(this.size.result).fixed(0), " 🔺"),
      t("Strength    ", t(this.strength.result).fixed(0), " 💪"),
      t("Condition   ", t(this.condition.result).fixed(0), " 🏃‍"),
      t("Flexibility ", t(this.flexibility.result).fixed(0), " 💃"),
      t("Intelligent ", t(this.intelligent.result).fixed(0), " ♟"),
      t("Charisma    ", t(this.charisma.result).fixed(0), " 🗣 "),
      t("Wisdom      ", t(this.wisdom.result).fixed(0), " 📚"),
      t("Might       ", t(this.might.result).fixed(0), " ❗️"),
      t(""),
      this.labelDescription
    ).list;
  }

  showCalculation() {
    const _this = this;

    return t(
      this.showBanner,
      t("Max HP      ", this.maxHP.toMimerText, " ❤️"),
      t("Size        ", this.size.toMimerText, " 🔺"),
      t("Strength    ", this.strength.toMimerText, " 💪"),
      t("Condition   ", this.condition.toMimerText, " 🏃‍"),
      t("Flexibility ", this.flexibility.toMimerText, " 💃"),
      t("Intelligent ", this.intelligent.toMimerText, " ♟"),
      t("Charisma    ", this.charisma.toMimerText, " 🗣 "),
      t("Wisdom      ", this.wisdom.toMimerText, " 📚"),
      t("Might       ", this.might.toMimerText, " ❗️")
    ).list;
  }

  damage(action: Effect): Effect {
    return action;
  }

  roll() {
    this.size.roll();
    this.condition.roll();
    this.strength.roll();
    this.flexibility.roll();
    this.intelligent.roll();
    this.charisma.roll();
    this.wisdom.roll();
    this.might.roll();
    this.hp = this.maxHP.result;
  }
}
