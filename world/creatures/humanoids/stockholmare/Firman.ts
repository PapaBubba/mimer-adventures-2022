"use strict";
import { t, Text } from "@mimer/text";
import { c } from "@mimer/calculation";
import Humanoid from "../humanoid";

export default function Firman(base: any) {
  return class Firman extends base {
    static creators = ["Achtung06"];
    static value = 2000;

    constructor(...args: any[]) {
      super(args[0] as string);
      this.cost *= 1.5;
      this.size.mult.value(2);
      this.condition.mult.value(2);
      this.strength.mult.value(4);
      this.flexibility.sub.value(4);
      this.intelligent.add.value(5);
      this.charisma.sub.value(4);
      this.wisdom.add.value(5);
    }

    static info(): Text {
      return t("En livsfarlig medlem i Firman Boys!").bold.red;
    }
  };
}
