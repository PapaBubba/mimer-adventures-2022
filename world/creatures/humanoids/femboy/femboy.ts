"use strict";
import { t, Text } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";
import { BattelAction, Occasion } from "../../../../game/action";
import Creature from "../../creature";
import Humanoid from "../humanoid";

export default class Astral extends Humanoid {
  static creators = ["Eldafint"];
  static value = 250;

  constructor(name: string, type: string[] = [], creators = Astral.creators) {
    super(name, ["Femboy"].concat(type), creators);
    this.cost = this.cost + Astral.value;
    this.size = c()
      .value(7)
      .add.dice(0, 3);
    this.condition = c()
      .value(15)
      .add.dice(0, 20);
    this.strength = c()
      .value(8)
      .add.dice(0, 10);
    this.flexibility = c()
      .value(20)
      .add.dice(0, 20);
    this.intelligent = c()
      .value(10)
      .add.dice(0, 15);
    this.charisma = c()
      .value(20)
      .add.dice(0, 10);
    this.wisdom = c()
      .value(10)
      .add.dice(0, 10);
    this.might = c()
      .value(10)
      .add.dice(0, 10);
    this.maxHP = c(this)
      .value(35)
      .add.property("size")
      .add.property("condition");
    this.hp = NaN;

    this._description = t("Är det en man? Är det en kvinna?");
    this._salute = t("Jag är en manlig man!");
    this._lose = t("Jag vägrar att förlora!");
    this._ask = t("Gillar du min kjol?");
    this._win = t("Nyaa!");
    this._title = t();
  }

  hit(): BattelAction {
    const my_action = new BattelAction(this, "En piruett", "förvirrad");
    const my_occation = my_action.createOccasion();
    const my_effect = my_occation.createEffect();

    my_action.tags.push("body");
    my_action.initiativ.dice(1, 10).add.property("condition");
    my_action.description = t("En snabb piruett för att visa sin kjol, ger inte mycket skada men förvirrar dess motståndare");

    my_occation.accuracy
      .value(0.7)
      .add.dice(0, 0.3)
      .percent.value(80);

    my_effect.type = "confound";
    my_effect.duration = c().value(3);

    return my_action;
  }

  selection(): BattelAction[] {
    let result = super.selection();
    result.push(this.magicTrick());
    this.numberOfSelections++;
    return result;
  }


  static infoChooseMe(): Text {
    return t("Välj en manlig man!").red;
  }

  static info() {
    return t("En manlig man!").blue;
  }
}
