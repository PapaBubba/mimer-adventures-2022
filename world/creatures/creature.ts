"use strict";
import { t, Text, TextToString } from "@mimer/text";
import { c, Calculation } from "@mimer/calculation";

import Resource from "../resource";
import { BattelAction } from "../../game/battelAction";
import { Effect } from "../../game/effect";
import { Occasion } from "../../game/occasion";

/**
 * The Calculation class represent calculation.
 */
export default abstract class Creature extends Resource {
  static creators = ["krm"];
  static value = 0;
  hp = 0;
  maxHP!: Calculation;
  type: string[];
  numberOfSelections = 0;
  protected _title!: Text;
  protected _description!: Text;
  protected _salute!: Text;
  protected _ask!: Text;
  protected _lose!: Text;
  protected _win!: Text;

  constructor(name: string, type: string[], creators = Creature.creators) {
    super(name, creators);
    this.cost = this.cost + Creature.value;
    this.type = type;
  }

  get labelName(): Text {
    if (TextToString(this._title) !== "") {
      return t(TextToString(this._title), " ", this.name).green; // Bad fix of bug.
    } else {
      return t(this.name).green;
    }
  }

  get labelType(): Text {
    return t(this.type.join("/"));
  }

  get labelAuthors(): Text {
    return t("Skapat av " + this.creators.join(", "));
  }

  get labelDescription(): Text {
    return this._description;
  }

  get sayName(): Text {
    return t(this.labelName, t(": ").bold);
  }

  get saySalute(): Text {
    return t(this.sayName, this._salute);
  }

  get sayQuestion(): Text {
    return t(this.sayName, this._ask);
  }

  get sayWin(): Text {
    return t(this.sayName, this._win);
  }

  get sayLose(): Text {
    return t(this.sayName, this._lose);
  }

  get showTitel(): Text {
    return this.labelName.headline;
  }

  get showBanner(): Text {
    return t(
      this.showTitel,
      this.labelType, " ",
      this.labelAuthors, "\n"
    ).bold;
  }

  get showStatus(): Text {
    if (this.isFighting()) {
      return t(
        this.sayName,
        t(String(this.hp)).bold,
        "/",
        String(this.maxHP.result),
        " ❤️"
      );
    } else {
      return t(this.sayName, " 🙁 ");
    }
  }

  static showChooseMe(): Text {
    return t("No info");
  }

  static createRandom(): any {
    return null;
  }

  isFighting(): boolean {
    return this.hp > 0;
  }

  selection(): BattelAction[] {
    this.numberOfSelections = 0;
    return [];
  }

  do(occasion: Occasion): Text {
    let roll = c().dice(1, 100).roll().result / 100.0;
    occasion.roll();
    let result = t();
    if (roll < occasion.accuracy.result) {
      let effect = occasion.effect[0];
      effect.roll();
      result
        .add(occasion.showDamageLabel)
        .add(
          t(
            "Aj, jag tog ",
            String(effect.value.result),
            " ❤️  i skada. Chansen var ",
            t(String(occasion.accuracy.result)).percent,
            " 🎯"
          ).newline
        );
      this.hp -= effect.value.result;
      if (!this.isFighting()) {
        result.add(this.sayLose.newline);
        result.add(occasion.action.by.sayWin.newline);
      }
    } else {
      result
        .add(occasion.showDamageLabel)
        .add("Ha! Du missade! Chansen var ")
        .add(t(String(occasion.accuracy.result)).percent)
        .add(" 🎯").newline;
    }
    return result;
  }

  abstract showStats(): Text;

  abstract damage(action: Effect): Effect;

  abstract roll(action: void): void;
}
