# Mimer Äventry

Ett lättsamt rollspel på svenska i en absurd fantasivärld.

I första hand tänkt som en programmeringsövning.

**Not completed yet**

## Install


```
$ npm install @mimer/text
```
## Usage


```
$ mag info
```



## Changelog

 * **0.1.0** *2022-03-24* Published first version, but it dose not work.

## License

AGPLv3, see LICENSE file

## Author

**© 2015-2022 [Magnus Kronnäs](https://magnus.kronnas.se)**
