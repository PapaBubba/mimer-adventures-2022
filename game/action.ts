"use strict";
import { t, Text } from "@mimer/text";
import Creature from "../world/creatures/creature";

/**
 * The  Action class represent action.
 */
export interface InputType<T> {
  prompt?: Text;
  name?: keyof T;
}

/**
 * Represents an action in the game
 */
export abstract class Action {
  name: string;
  verb: string;
  tags: string[] = [];
  description = t("");
  by: Creature;
  byPlayer: string = "";

  /**
   * The constructor for a Action
   *
   * @param by the creature doing the action.
   * @param name the of the action.
   * @param verb a verb descriping the action.
   */
  constructor(by: Creature, name = "", verb = "") {
    this.name = name;
    this.verb = verb;
    this.by = by;
  }

  /**
   * Make a short text for the name of action.
   *
   * @return a text of the name.
   */
  get showName(): Text {
    if (this.name !== "") {
      return t(this.name);
    } else {
      return t("Aktion");
    }
  }

  /**
   * Make a short text give a short description of the action as a label.
   *
   * @return a text of the label.
   */
  get showLabel(): Text {
    return t(this.showName.green, " > ").span(24);
  }
}
